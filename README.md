# AWSServerlessWebsocketChat

Simple stateless web-chat made using serverless AWS technologies: API Gateway web-sockets, Lambda and S3 as web-hosting.

URL: http://chat-websockets.s3-website.eu-central-1.amazonaws.com/

To start messaging follow the link above and send appeared invitation link to your interlocutor or simply open it in a new browser tab.
