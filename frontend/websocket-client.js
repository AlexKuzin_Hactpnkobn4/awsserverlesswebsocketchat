document.addEventListener('DOMContentLoaded', function(){

	const websocketClient = new WebSocket("wss://90wuc2gr00.execute-api.eu-central-1.amazonaws.com/production");

	const scroller_container = document.querySelector("#scroller_container")
	const messageInput = document.getElementById("message_input")
	const onSendMessage = () =>{
		if(receiverConnectionId != null && messageInput.value){
			const request = new Map([
				['action', 'getOrSendMessage'],
				['message', messageInput.value],
				['receiverConnectionId', receiverConnectionId],
			]);
			websocketClient.send(JSON.stringify(Object.fromEntries(request)))

			const messageContainer = document.createElement("div");
			messageContainer.classList.add("message_container","my_message")

			const messageText = document.createElement("p");
			messageText.classList.add("message_text")
			messageText.innerText = messageInput.value

			const timestamp = document.createElement("p");
			timestamp.classList.add("timestamp")
			const time = new Date()
			timestamp.innerText = time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds()

			messageContainer.appendChild(messageText)
			messageContainer.appendChild(timestamp)

			scroller_container.appendChild(messageContainer)
			if (scroller_container.scrollHeight - scroller_container.offsetHeight - scroller_container.scrollTop <= scroller_container.offsetHeight) {
				scroller_container.scrollTop = scroller_container.scrollHeight
			}
			messageInput.value = ""
		}
	}
	messageInput.addEventListener("keyup", function(event) {
		if (event.key === "Enter") {
			onSendMessage()
		}
	});

	const sendMessageButton = document.getElementById("send_button")

	const popupBg = document.querySelector(".popup_bg")
	const chatLinkContainer = document.querySelector(".link")

	let receiverConnectionId = new URLSearchParams(window.location.search).get("receiverConnectionId")

	websocketClient.onopen = function (){
		websocketClient.send("{\"action\":\"getMyConnectionId\"}")
		if(receiverConnectionId != null){
			const request = new Map([
				['action', 'makeConnection'],
				['receiverConnectionId', receiverConnectionId],
			]);
			websocketClient.send(JSON.stringify(Object.fromEntries(request)))
			popupBg.classList.remove("active")
		}

		sendMessageButton.onclick = onSendMessage

		websocketClient.onmessage = function (message) {
			const messageData = JSON.parse(message.data)
			if(messageData.action === "getMyConnectionId"){
				chatLinkContainer.innerHTML = window.location.href.split('?')[0] + "?receiverConnectionId=" + messageData.connectionId
			} else if(messageData.action === "makeConnection"){
				receiverConnectionId = messageData.connectionId
				popupBg.classList.remove("active")
			} else if (messageData.action === "getOrSendMessage") {
				const messageContainer = document.createElement("div");
				messageContainer.classList.add("message_container","companion_message")

				const messageText = document.createElement("p");
				messageText.classList.add("message_text")
				messageText.innerText = messageData.message

				const timestamp = document.createElement("p");
				timestamp.classList.add("timestamp")
				const time = new Date()
				timestamp.innerText = time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds()

				messageContainer.appendChild(messageText)
				messageContainer.appendChild(timestamp)

				scroller_container.appendChild(messageContainer)
				if (scroller_container.scrollHeight - scroller_container.offsetHeight - scroller_container.scrollTop <= scroller_container.offsetHeight) {
					scroller_container.scrollTop = scroller_container.scrollHeight
				}
				receiverConnectionId=messageData.connectionId
			}
		}
	}

}, false);