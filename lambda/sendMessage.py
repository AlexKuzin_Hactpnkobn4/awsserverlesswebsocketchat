import json
import urllib3
import boto3
import os

client = boto3.client('apigatewaymanagementapi', endpoint_url="https://90wuc2gr00.execute-api.eu-central-1.amazonaws.com/production")

def lambda_handler(event, context):

    message=json.loads(event.get('body'))["message"]
    receiverConnectionId=json.loads(event.get('body'))["receiverConnectionId"]
    connectionId=event.get('requestContext').get('connectionId')

    dictionary = {
        "action": "getOrSendMessage",
        "message": message,
        "connectionId": connectionId
    }
    response = client.post_to_connection(ConnectionId=recieverConnectionId, Data=json.dumps(dictionary).encode('utf-8'))
    return {
        'statusCode': 200
    }
